import json
import os

import matplotlib.pyplot as plt
import numpy as np
import rdkit.Chem.Draw as Draw
import torchvision.transforms as transforms


def draw_molecules(molecules):
    img = Draw.MolsToGridImage(molecules, molsPerRow=5)
    tensor = transforms.ToTensor()(img)

    return tensor


def draw_bars(y1, y2):
    assert len(y1) == len(y2)
    fig, axs = plt.subplots(ncols=len(y1))
    for i, ax in enumerate(axs):
        ax.bar(.0, y1[i], color='g')
        ax.bar(1., y2[i], color='r')
        ax.set_xticklabels(['', 'true', 'fake', ''], rotation='vertical')
    plt.tight_layout()
    return fig


def get_model_version(model_name="molgan", logdir='tb_logs/', width=3):
    model_files = list(sorted([f for f in os.listdir(logdir) if f"{model_name}_v" in f]))
    if len(model_files) < 1:
        version = '1'.rjust(width, '0')
    else:
        last_version = int(model_files[-1][-width:])
        version = str(last_version + 1).rjust(width, '0')
    return version


def save_config(path, args):
    with open(os.path.join(path, "config.json"), "w") as json_f:
        json.dump(vars(args), json_f)


def read_config(path):
    with open(os.path.join(path, "config.json")) as json_f:
        return json.load(json_f)


def test_draw_molecules():
    from dataset import MolDataset
    ds = MolDataset("alkanes", source_file="data/alkanes.csv")

    mols = [ds[i] for i in range(30)]

    mols = [ds.convert_back(X, A) for _, X, A in mols]

    tensor = draw_molecules(mols)

    plt.imshow(np.transpose(tensor, (1, 2, 0)))
    plt.savefig('test.png')


if __name__ == '__main__':
    get_model_version()
