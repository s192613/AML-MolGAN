import os
import torch
import argparse
import utils
import numpy as np
import rdkit.Chem.Draw as Draw
from rdkit import Chem
from metrics import MolecularMetrics as metrics

from rgcn import RGCN
from generator import Generator
from train import prepare_data, set_seeds



def load_checkpoint(log_dir, epoch, config):
    if epoch == -1:
        epoch = max(map(lambda s: int(s[0:-4].split('_')[1]), [f for f in os.listdir(log_dir) if "checkpoint" in f]))

    fname = f"checkpoint_{epoch}.tar"
    path = os.path.join(log_dir, fname)

    checkpoint = torch.load(path)

    D = RGCN()
    D.load_state_dict(checkpoint["D_state_dict"])

    G = Generator()
    G.load_state_dict(checkpoint["G_state_dict"])

    R = RGCN(final_act=True)
    R.load_state_dict(checkpoint["R_state_dict"])

    opt_D = torch.optim.Adam(D.parameters(), lr=config['lr'],
                             betas=(config['beta1'], config['beta2']))
    opt_G = torch.optim.Adam(G.parameters(), lr=config['lr'],
                             betas=(config['beta1'], config['beta2']))
    opt_R = torch.optim.Adam(R.parameters(), lr=config['lr'],
                             betas=(config['beta1'], config['beta2']))

    opt_D.load_state_dict(checkpoint["opt_D_state_dict"])
    opt_G.load_state_dict(checkpoint["opt_G_state_dict"])
    opt_R.load_state_dict(checkpoint["opt_R_state_dict"])

    return (D, opt_D), (G, opt_G), (R, opt_R)


def parse_args():
    parser = argparse.ArgumentParser(description="script making plots.")
    parser.add_argument('-l', '--logdir', type=str, required=True,
                        help="path to the directory containing the checkpoints.")
    parser.add_argument('-e', '--epoch', type=int, help="epoch from which the stats should be generated. (default: last).", default="-1")
    return parser.parse_args()


if __name__ == '__main__':
    set_seeds()

    args = parse_args()
    config = utils.read_config(args.logdir)
    dataset, _ = prepare_data(config['data'], config['batch_size'])
    (D, _), (G, _), (R, _) = load_checkpoint(args.logdir, args.epoch, config)

    G = G.eval()
    D = D.eval()
    R = R.eval()

    z = G.sample_z(config['val_size'])
    Xs, As = G(z)

    sampled_mols = [dataset.convert_back(X, A, strict=True) for X, A in zip(Xs, As)]

    validity = metrics.valid_total_score(sampled_mols)
    novelty = metrics.novel_total_score(sampled_mols, dataset)
    uniqueness = metrics.unique_total_score(sampled_mols)
    solubility = metrics.water_octanol_partition_coefficient_scores(sampled_mols, norm=True).mean()

    print(f"Validity: {validity/config['val_size']} / {validity} ")
    print(f"Novelty: {novelty/validity} / {novelty}")
    print(f"Uniqueness: {uniqueness/validity} / {uniqueness}")
    print(f"Solubility: {solubility}")

    unique = [Chem.MolFromSmiles(s) for s in set(map(lambda x: Chem.MolToSmiles(x), metrics.valid_lambda_filter(sampled_mols)))]
    # molecules with fluorine

    qed = metrics.quantitative_estimation_druglikeness_scores(unique, dataset)
    idx = np.argsort(qed)[-64:]
    # Draw molecules
    draw_mols = [unique[i] for i in idx]

    legends = [f"{qed[i]:.3f}" for i in idx]
    img = Draw.MolsToGridImage(draw_mols, molsPerRow=8, legends=legends)
    img.save("no-rl.pdf")




