import os
import pickle
from itertools import repeat

import torch
from rdkit import Chem
from torch.utils import data


def make_infinite_loader(data_loader):
    """
    :param data_loader: (data.DataLoader)
    """
    for loader in repeat(data_loader):
        for batch in loader:
            yield batch


class MolDataset(data.Dataset):
    extension = ".mol_dataset"
    default_bonds = [Chem.rdchem.BondType.ZERO, Chem.rdchem.BondType.SINGLE, Chem.rdchem.BondType.DOUBLE,
                     Chem.rdchem.BondType.TRIPLE]
    default_atoms = [0, 6, 7, 8, 9]

    def __init__(self, name, force_reload=False, verbose=False, **build_kwargs):
        self.name = name
        self.verbose = verbose

        self.mols = None
        self.As = None
        self.Xs = None
        self.x_to_atom = None
        self.a_to_bond = None

        if not force_reload and self._check_exists():
            self._load_dataset()
        else:
            self._build_dataset(**build_kwargs)

        self.atoms_frequency = self.compute_atoms_frequency(self.Xs)
        self.bonds_frequency = self.compute_bonds_frequency(self.As)
        self.smiles = {Chem.MolToSmiles(s) for s in self.mols}  # useful in validation to avoid recomputing

    def __len__(self):
        return len(self.As)

    def __getitem__(self, idx):
        return idx, self.Xs[idx], self.As[idx]

    def _check_exists(self):
        return os.path.exists(self.name + self.extension)

    def _load_dataset(self):
        fname = self.name + self.extension
        with open(fname, "rb") as f:
            self.mols, self.Xs, self.As, self.x_to_atom, self.a_to_bond = pickle.load(f)
        self._echo(f"Data loaded from: {fname}.")

    def _save_dataset(self):
        fname = self.name + self.extension
        with open(fname, "wb") as f:
            pickle.dump((self.mols, self.Xs, self.As, self.x_to_atom, self.a_to_bond), f)
        self._echo(f"Data saved in: {fname}.")

    def _echo(self, msg):
        if self.verbose:
            print(msg)

    def _build_dataset(self, **build_kwargs):

        source_file = build_kwargs.pop("source_file")

        self._echo(f"Start building from source file: {source_file}.")

        if source_file.endswith('.sdf'):
            mols = Chem.SDMolSupplier(source_file)
        elif source_file.endswith('.csv') or source_file.endswith('.txt'):
            with open(source_file) as f_handle:
                smiles = f_handle.readlines()
                mols = [Chem.MolFromSmiles(smile, sanitize=True) for smile in smiles]
        else:
            raise NotImplementedError(f"Does not support that file extension: {source_file}.")

        # Discard None molecules
        mols = list(filter(lambda mol: mol is not None, mols))

        # Discard Aromatic
        mols = list(filter(
            lambda mol: Chem.rdchem.BondType.AROMATIC not in list(map(lambda b: b.GetBondType(), mol.GetBonds())),
            mols))

        # Mappings
        test_mode = build_kwargs.pop("test_mode", False)
        if test_mode:
            atoms = self.default_atoms
            bonds = self.default_bonds
        else:
            atoms = [0] + list(sorted(set([atom.GetAtomicNum() for mol in mols for atom in mol.GetAtoms()])))
            bonds = [Chem.rdchem.BondType.ZERO] + list(
                sorted(set([bond.GetBondType() for mol in mols for bond in mol.GetBonds()])))

        atom_to_idx = {a: i for i, a in enumerate(atoms)}
        self.x_to_atom = {i: a for i, a in enumerate(atoms)}

        bond_to_idx = {b: i for i, b in enumerate(bonds)}
        self.a_to_bond = {i: b for i, b in enumerate(bonds)}

        # Constants
        N = max(mol.GetNumAtoms() for mol in mols)
        T = len(atoms)
        Y = len(bonds)

        # Create matrices As and Xs for each mol
        Xs, As = [], []
        for k, mol in enumerate(mols):
            X = torch.zeros((N, T))
            X[:, 0] = True  # Fill with "empty" atom

            A = torch.zeros((N, N, Y))
            A[:, :, 0] = True  # Fill with "zero" bond

            for i, atom in enumerate(mol.GetAtoms()):
                atom_num = atom.GetAtomicNum()
                idx = atom_to_idx[atom_num]

                X[i, 0] = False  # Remove "empty" atom
                X[i, idx] = True

            for bond in mol.GetBonds():
                bond_type = bond.GetBondType()
                idx = bond_to_idx[bond_type]
                i, j = bond.GetBeginAtomIdx(), bond.GetEndAtomIdx()

                A[i, j, 0] = False  # Remove "zero" bond
                A[j, i, 0] = False

                A[i, j, idx] = True
                A[j, i, idx] = True

            Xs.append(X), As.append(A)

        self._echo("Done.")
        self.mols, self.Xs, self.As = mols, Xs, As
        self._save_dataset()

    def convert_back(self, X, A, strict=False):
        mol = Chem.RWMol()

        for x in X.argmax(dim=1):
            atom = self.x_to_atom[x.item()]
            mol.AddAtom(Chem.Atom(atom))

        Amax = A.argmax(dim=2)
        for i, j in torch.nonzero(Amax):
            if i < j:
                bond = self.a_to_bond[Amax[i, j].item()]
                if bond != Chem.BondType.ZERO:  # Discard "zero" bond
                    mol.AddBond(i.item(), j.item(), bond)
        if strict:
            try:
                Chem.SanitizeMol(mol)
            except:
                mol = None

        return mol

    @staticmethod
    def compute_atoms_frequency(Xs):
        return torch.stack([X.sum(dim=0) for X in Xs], dim=0).sum(dim=0) / len(Xs)

    @staticmethod
    def compute_bonds_frequency(As):
        # NOTE: Bonds are "doubled"
        return torch.stack([A.sum(dim=(0, 1)) for A in As], dim=0).sum(dim=0) / len(As)


def test_building(args):
    ds = MolDataset(args["name"], source_file=args["source_file"], verbose=True, force_reload=True,
                    **{"test_mode": True})
    n_mols = args["n"]
    assert len(ds) == n_mols, f"Data set should contain {n_mols}, it contains {len(ds)}."


def test_loading(args):
    ds = MolDataset(args["name"], source_file=args["source_file"], verbose=True, force_reload=False)
    n_mols = args["n"]
    print(ds.a_to_bond)
    print(ds.x_to_atom)
    assert len(ds) == n_mols, f"Data set should contain {n_mols}, it contains {len(ds)}."


def test_ext_mapping(args):
    ds = MolDataset(args["name"], source_file=args["source_file"], verbose=True, force_reload=True,
                    **{"test_mode": True})

    assert list(ds.a_to_bond.values()) == MolDataset.default_bonds
    assert list(ds.x_to_atom.values()) == MolDataset.default_atoms


def test_conversion(args):
    ds = MolDataset(args["name"], source_file=args["source_file"], verbose=True, **{"test_mode": True})

    idx, X, A = ds[1]
    mol = ds.convert_back(X, A)

    true_atoms, recovered_atoms = [atom.GetAtomicNum() for atom in ds.mols[idx].GetAtoms()], [atom.GetAtomicNum() for
                                                                                              atom in mol.GetAtoms()]
    true_bonds, recovered_bonds = [(bond.GetBondType(), bond.GetBeginAtomIdx(), bond.GetEndAtomIdx()) for bond in
                                   ds.mols[idx].GetBonds()], \
                                  [(bond.GetBondType(), bond.GetBeginAtomIdx(), bond.GetEndAtomIdx()) for bond in
                                   mol.GetBonds()]

    assert mol.HasSubstructMatch(ds.mols[idx]) and ds.mols[idx].HasSubstructMatch(
        mol), f"Recovered molecule does not match the true one: {recovered_atoms, recovered_bonds} vs {true_atoms, true_bonds} ."


def test_with_dataloader(args):
    from torch.utils.data import DataLoader
    ds = MolDataset(args["name"], source_file=args["source_file"], verbose=True, force_reload=False)
    dl = DataLoader(ds, batch_size=32, shuffle=True)
    print(len(ds))
    for batch_idx, batch in enumerate(dl):
        if batch_idx > 1: break
        print(batch_idx)
        idx, Xs, As = batch[0], batch[1], batch[2]
        print(idx, len(Xs), len(As))


def test_infinite_dataloader(args):
    from torch.utils.data import DataLoader
    ds = MolDataset(args["name"], source_file=args["source_file"], verbose=True, force_reload=False)
    dl = DataLoader(ds, batch_size=32, shuffle=True)
    data_loader = make_infinite_loader(dl)
    i = 0
    expected_batch_nb = args["n"] // 32 + 1
    for i, batch in enumerate(data_loader):
        if i > 100:
            break
    assert i > expected_batch_nb, f"At least {expected_batch_nb} batches were expected, got only {i}."


def test_stats(args):
    import utils
    ds = MolDataset(args["name"], source_file=args["source_file"], verbose=True, force_reload=False)
    utils.draw_bars(ds.atoms_frequency, ds.atoms_frequency)


if __name__ == '__main__':
    qm9 = {"name": "qm9", "source_file": "data/gdb9.sdf", "n": 111248}
    qm9_5k = {"name": "qm9-5k", "source_file": "data/qm9_5k.csv", "n": 4260}
    alkanes = {"name": "alkanes", "source_file": "data/alkanes.csv", "n": 1073}

    # test_building(qm9)
    # test_loading(qm9)
    # test_ext_mapping(qm9)
    # test_conversion(alkanes)
    # test_infinite_dataloader(alkanes)
    test_stats(qm9_5k)
