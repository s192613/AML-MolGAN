import torch
import torch.nn as nn
import torch.nn.functional as F


class Generator(nn.Module):
    METHODS = ("continuous", "gumbel", "gumbel-softmax")

    def __init__(self, z_dim=32, hidden_dims=(128, 256, 512), method="gumbel-softmax", dropout_rate=.1):
        super(Generator, self).__init__()

        assert method in self.METHODS, f"Only the following methods {self.METHODS} are supported."

        self.z_dim = z_dim

        self.N, self.T, self.Y = 9, 5, 4
        self.method = method

        self.activation = nn.Tanh()
        self.layers = nn.ModuleList(
            [nn.Linear(in_dim, out_dim) for in_dim, out_dim in zip((z_dim,) + hidden_dims, hidden_dims)])

        self.to_X = nn.Linear(hidden_dims[-1], self.N * self.T)
        self.to_A = nn.Linear(hidden_dims[-1], self.N * self.N * self.Y)

        self.dropout = nn.Dropout(p=dropout_rate)

    def forward(self, z):
        for layer in self.layers:
            z = layer(z)
            z = self.activation(z)
            z = self.dropout(z)

        # Linear Projection
        X = self.to_X(z)

        A = self.to_A(z)

        # Reshape
        X = X.view((-1, self.N, self.T))

        A = A.view((-1, self.N, self.N, self.Y))

        # Output of the network
        if self.method == "continuous":  # Method i) in the paper
            X = F.softmax(X, dim=2)
            A = F.softmax(A, dim=3)

        elif self.method == "gumbel":  # Method ii) in the paper (Gumbel Noise added)
            X = F.gumbel_softmax(X, dim=2)
            A = F.gumbel_softmax(A, dim=3)

        else:  # Method iii) in the paper (Gumbel Softmax, i.e. one-hot in the forward but continuous in the backward)
            X = F.gumbel_softmax(X, dim=2, hard=True)
            A = F.gumbel_softmax(A, dim=3, hard=True)

        return X, A

    def sample_z(self, batch_size):
        z = torch.randn(batch_size, self.z_dim)
        return z


def test_generator():
    generator = Generator(method="gumbel-softmax")
    z = generator.sample_z(10)
    X, A = generator(z)
    print(X)


if __name__ == '__main__':
    test_generator()
