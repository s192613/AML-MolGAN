import argparse
import os
import numpy as np
import matplotlib.pyplot as plt
import matplotlib as mpl
from tensorboard.backend.event_processing.event_accumulator import EventAccumulator

plt.style.use('classic')

# Rendering options for the plots
PLOT_CONTEXT = {
    'font.family': 'serif',
    'font.serif': 'Computer Modern Sans',
    'text.usetex': True,
    'font.size': 14,
    'xtick.labelsize': 14,
    'ytick.labelsize': 14,
    'axes.formatter.useoffset': False
}

# Plots that can be made from tensorboard logs
# NOTE: 'tb_fields' is a dict 'tensorboard scalar' -> 'legend'
TB_PLOTS = {'metrics': {"xlabel": "epochs",
                        "ylabel": "\# molecules",
                        "axvline": {"x": 150, "label":"End of pre-training", "color":'k', "linestyle": '--'},
                        "tb_fields": {'Metrics/novelty': '$n_{novel}$',
                                      'Metrics/validity': '$n_{valid}$',
                                      'Metrics/uniqueness': '$n_{unique}$'},
                        "ylim": (-50, 6450)},
            'reward': {"xlabel": "batches",
                       "ylabel": "reward",
                        "axvline": {"x": 20000, "label":"End of pre-training", "color":'k', "linestyle": '--'},
                       "tb_fields": {'Rewards/fake': 'fake'},
                       "ylim": (0, 1)},
            }


def plot_tb_logs(tb_path, plots_path, plot_context=None):
    if plot_context is None:
        plot_context = PLOT_CONTEXT

    event_acc = EventAccumulator(tb_path)
    event_acc.Reload()

    with mpl.rc_context(plot_context):
        for pname in TB_PLOTS:

            pdict = TB_PLOTS[pname]
            rdict = {}

            # get the fields in the event file
            for field in pdict["tb_fields"]:
                _, steps, vals = zip(*event_acc.Scalars(field))
                rdict[field] = {"steps": steps, "vals": vals}

            # plot the corresponding figure
            f = plt.figure()
            plt.xlabel(pdict['xlabel'])
            plt.ylabel(pdict['ylabel'])

            for field in pdict["tb_fields"]:
                plt.plot(rdict[field]["steps"], rdict[field]["vals"])

            if len(pdict["tb_fields"]) > 1:  # legend only if multiple lines
                labels = list(pdict["tb_fields"].values())
                plt.legend(labels, loc='upper left')

            if "ylim" in pdict:
                plt.ylim(pdict["ylim"])
            if "axvline" in pdict:
                plt.axvline(**pdict["axvline"])
                y = plt.gca().get_ylim()[1]
                plt.text(0.95*pdict["axvline"]["x"], .67*y, pdict["axvline"]["label"], rotation=90)

            plt.tight_layout()
            f.savefig(f"{plots_path}/{pname}.pdf")
            plt.close(f)


def parse_args():
    parser = argparse.ArgumentParser(description="script making plots.")
    parser.add_argument('-l', '--logdir', type=str, required=True,
                        help="path to the directory containing the necessary files for plotting")
    return parser.parse_args()


if __name__ == '__main__':
    args = parse_args()
    plots_path = os.path.join(args.logdir, "plots")
    os.makedirs(plots_path, exist_ok=True)

    plot_tb_logs(args.logdir, plots_path)
