import argparse
import os
import random

import numpy as np
import torch
from torch.utils.data import DataLoader
from torch.utils.tensorboard import SummaryWriter
from tqdm import tqdm

import utils
from dataset import MolDataset
from generator import Generator
from metrics import MolecularMetrics as metrics
from rgcn import RGCN


def parse_cmd():
    parser = argparse.ArgumentParser(description="script to train MolGAN model.")
    parser.add_argument('--model_name', type=str, default='molgan-rgcn2',
                        help='name given to the mode (default: molgan')
    parser.add_argument('--batch_size', type=int, default=32, help='input batch size for training (default: 32)')
    parser.add_argument('--n_epochs', type=int, default=300, help='number of training epochs (default: 300)')
    parser.add_argument('--pre_training', type=int, default=150,
                        help='number of training epochs used to pre-train the generator, without the RL-loss (default: 150)')
    parser.add_argument('--lr', type=float, default=5e-4, help='Adam learning rate (default: 5e-4)')
    parser.add_argument('--beta1', type=float, default=0., help='Adam beta 1 (default: 0.5)')
    parser.add_argument('--beta2', type=float, default=0.9, help='Adam beta 2 (default: 0.9)')
    parser.add_argument('--n_critic', type=int, default=5,
                        help='number of discriminator iterations before generator updated (default: 5)')
    parser.add_argument('--val_size', type=int, default=6400,
                        help='number of sample molecules to generate at evaluation time (default: 6400)')
    parser.add_argument('--alpha', type=float, default=10.,
                        help='gradient penalty coefficient (default: 10.)')
    parser.add_argument('--data', type=str, choices=['alkanes', 'qm9-5k', 'qm9'], default='qm9-5k',
                        help='Data to be used to train the molGAN, can be a debug dataset, '
                             'a 5k-subset of QM9, or the full QM9. (default: debug.)')
    parser.add_argument('--logdir', type=str, default='tb_logs',
                        help='directory where the tensorboard logs will be located (default:tb_logs)')
    parser.add_argument('--discr_dropout_rate', type=float, default=.1,
                        help='dropout rate to be applied in the discriminator at training time. (default: .1)')
    parser.add_argument('--gen_dropout_rate', type=float, default=.1,
                        help='dropout rate to be applied in the generator at training time. (default: .1)')
    parser.add_argument('--trade_off', type=float, default=1.,
                        help='trade-off between WGAN and RL loss (default: 1., i.e. only WGAN loss)')
    parser.add_argument('--rl_objectives', type=str, nargs='+', choices=['logp', 'validity', 'unique', 'sas'],
                        default=['logp'])
    parser.add_argument('--save_every', type=int, default=10)

    args = parser.parse_args()

    return args


def set_seeds(seed=42):
    torch.manual_seed(seed)
    np.random.seed(seed)
    random.seed(seed)


def prepare_data(name, batch_size):
    if name == "alkanes":
        dataset = MolDataset("alkanes", source_file="data/alkanes.csv", test_mode=True,
                             force_reload=True)
    elif name== "qm9-5k":
        dataset = MolDataset("qm9-5k", source_file="data/qm9_5k.csv", force_reload=True)
    else:
        dataset = MolDataset("qm9", source_file="data/gdb9.sdf")

    return dataset, DataLoader(dataset, batch_size=batch_size, shuffle=True)


if __name__ == '__main__':
    set_seeds()
    args = parse_cmd()
    dataset, data_loader = prepare_data(args.data, args.batch_size)
    D = RGCN(dropout_rate=args.discr_dropout_rate)
    G = Generator(dropout_rate=args.gen_dropout_rate)
    R = RGCN(dropout_rate=args.discr_dropout_rate, final_act=True)  # activation

    opt_D = torch.optim.Adam(D.parameters(), lr=args.lr,
                             betas=(args.beta1, args.beta2))
    opt_G = torch.optim.Adam(G.parameters(), lr=args.lr,
                             betas=(args.beta1, args.beta2))
    opt_R = torch.optim.Adam(R.parameters(), lr=args.lr,
                             betas=(args.beta1, args.beta2))

    version = utils.get_model_version(args.model_name, args.logdir)
    log_path = os.path.join(args.logdir, args.model_name + f"_v{version}")
    writer = SummaryWriter(log_path, flush_secs=30)
    utils.save_config(log_path, args)

    for epoch in range(args.n_epochs):

        # Training
        G = G.train()
        D = D.train()
        R = R.train()

        for i, batch in tqdm(enumerate(data_loader), total=len(dataset) // args.batch_size + 1,
                             desc=f"Epoch {epoch + 1}/{args.n_epochs}"):

            global_step = (epoch * len(dataset) // args.batch_size + 1) + i

            # Train the discriminator
            for p in D.parameters():
                p.requires_grad = True

            # get the data
            true_idx, true_X, true_A = batch
            batch_size = true_X.shape[0]

            # score the true data
            score_true = D(true_X, true_A).mean()

            # score the fake data
            z = G.sample_z(batch_size)
            X_fake, A_fake = G(z)
            # NOTE: remove from the generated data from computational graph to avoid backprop through the generator
            X_fake.detach()
            A_fake.detach()
            score_fake = D(X_fake, A_fake).mean()

            # compute gradient penalty
            # Make linear combination examples
            eps = torch.rand(batch_size)
            eps_x = eps.view(-1, 1, 1)
            X_hat = eps_x * true_X + (1. - eps_x) * X_fake.detach()  # NOTE: detach from graph
            X_hat.requires_grad_()

            eps_a = eps.view(-1, 1, 1, 1)
            A_hat = eps_a * true_A + (1. - eps_a) * A_fake.detach()  # NOTE: detach from graph
            A_hat.requires_grad_()

            # Forward examples
            d_x_hat = D(X_hat, A_hat)
            # XXX: Important to provide grad_outputs, otherwise gradients will be summed over batch dimension.
            #      Eq (6) is given for one training example, so that the L-2 norm must be computed per-example,
            #      not on the sum of all the gradients.
            X_grads, A_grads = torch.autograd.grad(outputs=d_x_hat, inputs=[X_hat, A_hat],
                                                   grad_outputs=torch.ones((batch_size, 1)), create_graph=True,
                                                   retain_graph=True, only_inputs=True)

            X_penalty = torch.mean((X_grads.norm(dim=-1) - 1.) ** 2, dim=-1)
            A_penalty = torch.mean((A_grads.norm(dim=-1) - 1.) ** 2, dim=(-2, -1))

            penalty = (X_penalty + A_penalty).mean()

            d_loss = -score_true + score_fake + args.alpha * penalty
            opt_D.zero_grad()
            d_loss.backward()
            opt_D.step()

            # Logging
            writer.add_scalar('Loss/discriminator', d_loss, global_step=global_step)
            writer.add_scalar('Score/true', score_true, global_step=global_step)
            writer.add_scalar('Score/fake', score_fake, global_step=global_step)
            writer.add_scalar('Penalty/X', X_penalty.mean(), global_step=global_step)
            writer.add_scalar('Penalty/A', A_penalty.mean(), global_step=global_step)
            writer.add_scalar('Penalty/Joint', penalty, global_step=global_step)

            if (i + 1) % args.n_critic == 0:
                for p in D.parameters():
                    p.requires_grad = False

                # Train the generator
                z = G.sample_z(args.batch_size)
                X_fake, A_fake = G(z)

                # score with respect to the discriminator
                score_fake = D(X_fake, A_fake).mean()

                if args.trade_off < 1.:  # train the reward network
                    # compute labels
                    true_mols = [dataset.mols[idx] for idx in true_idx]
                    true_reward = metrics.reward(true_mols, dataset, args.rl_objectives)
                    # compute reward of the true mols.
                    fake_mols = [dataset.convert_back(X, A, strict=True) for X, A in zip(X_fake, A_fake)]
                    fake_reward = metrics.reward(fake_mols, dataset, args.rl_objectives)

                    true_reward_pred = R(true_X, true_A)
                    fake_reward_pred = R(X_fake, A_fake)

                    # optimize reward network
                    r_loss = torch.mean((true_reward_pred - true_reward) ** 2 + (
                            fake_reward_pred - fake_reward) ** 2)  # MSE between true and
                    opt_R.zero_grad()
                    r_loss.backward(retain_graph=True)
                    opt_R.step()

                    writer.add_scalar('Rewards/fake', fake_reward.mean(),
                                      global_step=global_step)
                    writer.add_scalar('Loss/reward', r_loss, global_step=global_step)

                if args.trade_off < 1. and epoch > args.pre_training:
                    g_loss = args.trade_off * (-score_fake) + (1 - args.trade_off) * (-fake_reward_pred.mean())
                else:
                    g_loss = (-score_fake)

                opt_G.zero_grad()
                g_loss.backward()
                opt_G.step()
                writer.add_scalar('Loss/generator', g_loss, global_step=global_step)

        #  --- validation ---
        G = G.eval()
        D = D.eval()
        R = R.eval()

        z = G.sample_z(args.val_size)
        Xs, As = G(z)

        sampled_mols = [dataset.convert_back(X, A, strict=True) for X, A in zip(Xs, As)]

        # Draw molecules
        draw_mols = []
        i = 0
        while len(draw_mols) <= 25 and i < len(sampled_mols):
            if sampled_mols[i] is not None:
                draw_mols.append(sampled_mols[i])
            i += 1

        if len(draw_mols):
            img = utils.draw_molecules(draw_mols)
            writer.add_image('sampled molecules', img, epoch)

        writer.add_scalar('Metrics/validity', metrics.valid_total_score(sampled_mols), epoch)
        writer.add_scalar('Metrics/uniqueness', metrics.unique_total_score(sampled_mols), epoch)
        writer.add_scalar('Metrics/novelty', metrics.novel_total_score(sampled_mols, dataset), epoch)
        writer.add_figure('Statistics/atoms-distribution',
                          utils.draw_bars(dataset.atoms_frequency, dataset.compute_atoms_frequency(Xs.detach())), epoch)
        writer.add_figure('Statistics/bonds-distribution',
                          utils.draw_bars(dataset.bonds_frequency, dataset.compute_bonds_frequency(As.detach())), epoch)
        # writer.add_scalar('Metrics/solubility', metrics.solubility_score(sampled_mols), val_step)

        # --- saving ---
        if (epoch + 1) % args.save_every == 0:
            torch.save({"D_state_dict": D.state_dict(),
                        "G_state_dict": G.state_dict(),
                        "R_state_dict": R.state_dict(),
                        "opt_D_state_dict": opt_D.state_dict(),
                        "opt_G_state_dict": opt_G.state_dict(),
                        "opt_R_state_dict": opt_R.state_dict()
                        },
                       os.path.join(log_path, f"checkpoint_{epoch}.tar"))

    writer.close()
