import torch
import torch.nn as nn


class RGCN(nn.Module):
    def __init__(self, dropout_rate=.1, final_act=False):
        super(RGCN, self).__init__()

        self.N, self.T, self.Y = 9, 5, 4

        # Graph Convolution
        self.fy_0 = nn.ModuleList([nn.Linear(self.T, 64) for _ in range(self.Y)])
        self.fs_0 = nn.Linear(self.T, 64)

        self.fy_1 = nn.ModuleList([nn.Linear(64 + self.T, 32) for _ in range(self.Y)])
        self.fs_1 = nn.Linear(64 + self.T, 32)

        self.conv_activation = nn.Tanh()

        # Graph Aggregation
        self.i = nn.Linear(32 + self.T, 128)
        self.j = nn.Linear(32 + self.T, 128)
        self.i_activation = nn.Sigmoid()
        self.j_activation = nn.Tanh()
        self.agg_activation = nn.Tanh()

        # Final MLP
        self.mlp_activation = nn.Tanh()
        self.mlp_layer0 = nn.Linear(128, 128)
        self.mlp_layer1 = nn.Linear(128, 1)
        self.final_activation = nn.Sigmoid() if final_act else None

        # Dropout
        self.dropout = nn.Dropout(p=dropout_rate)

    def forward(self, X, A):
        """
        :param X: (B, N, T) tensor
        :param A: (B, N, N, Y)
        :return:
        """
        # Normalization Factor
        # (B, N)
        Ni = A
        # --- Convolution ---
        # Put the Y dimension in second position for batch matrix multiplication with the embeddings
        A = A[:, :, :, :].permute(0, 3, 1, 2)

        # First layer, no concatenation as no hidden tensor yet
        embeddings = torch.stack([self.fy_0[y](X) for y in range(self.Y)], dim=1)  # dim=1, y also in dim=1 in A

        Afy_0 = torch.matmul(A, embeddings)

        H = torch.sum(Afy_0, dim=1) + self.fs_0(X)  # dim=1, sum over y in equation (5.1)
        H = self.dropout(self.conv_activation(H))

        # Second layer
        # Concatenate with the hidden state, i.e. (h_{i}, x_{i})
        HiXi = torch.cat((H, X), dim=-1)

        # (B, i, j, 69)
        HjXi = torch.stack(
            [torch.stack([torch.cat((H[:, j, :], X[:, i, :]), dim=-1) for j in range(self.N)], dim=1) for i in
             range(self.N)], dim=1)

        # (B, y, i, j, 32)
        embeddings = torch.stack([self.fy_1[y](HjXi) for y in range(self.Y)], dim=1)

        # (B, y, i, j, 32)
        Afy_1 = A.unsqueeze(4) * embeddings

        # dim=1, sum over (j, y) in equation (5.1)
        H = torch.sum(Afy_1, dim=(1, 3)) + self.fs_1(HiXi)
        H = self.dropout(self.conv_activation(H))

        # --- Aggregation ---
        H = torch.cat((H, X), dim=-1)
        i = self.dropout(self.i_activation(self.i(H)))
        j = self.dropout(self.j_activation(self.j(H)))

        Hg = self.agg_activation(torch.sum(i * j, dim=1))  # dim=1, sum over v in equation (6.1)

        # --- Final MLP --
        out = self.dropout(self.mlp_activation(self.mlp_layer0(Hg)))
        out = self.mlp_layer1(out)

        if self.final_activation:
            out = self.final_activation(out)

        return out


if __name__ == '__main__':
    from generator import Generator

    D = RGCN(final_act=True)
    G = Generator()
    z = G.sample_z(10)
    X, A = G(z)

    print(D(X, A))
